package br.com.itau;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        int lados = 6;

        System.out.println("*** Sortear um número entre 1 e 6 ***");
        int numero1 = random.nextInt(lados) + 1;
        System.out.println("Número sorteado: " + numero1);
        System.out.println(" ");


        int sorteios2 = 3;
        int soma2 = 0;

        System.out.println("*** Sortear " + sorteios2 + " números entre 1 e 6, e somá-los ***");
        System.out.print("Números sorteados: ");
        for(int i = 1; i <= sorteios2; i++){
            int numero2 = random.nextInt(lados) + 1;
            System.out.print(numero2);
            if(i <= sorteios2 - 1){
                System.out.print(", ");
            } else
                System.out.println(" ");
            soma2 = soma2 + numero2;
        }
        System.out.println("Soma dos números: " + soma2);
        System.out.println(" ");


        int sorteios3 = 3;
        int grupoSorteio3 = 3;
        int soma3 = 0;

        System.out.println("*** Sortear e somar " + sorteios3 + " números, " + grupoSorteio3 + " vezes ***");
        for(int j = 1; j <= grupoSorteio3; j++){
            System.out.print("Números do sorteio " + j + ", com a soma: ");
            for(int k = 1; k <= sorteios3; k++){
                int numero3 = random.nextInt(lados) + 1;
                System.out.print(numero3);
                System.out.print(", ");
                soma3 = soma3 + numero3;
            }
            System.out.println(soma3);
            soma3 = 0;
        }
    }
}
